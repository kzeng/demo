﻿Ext.define('app.store.QuizList', {
    extend: 'Ext.data.Store',
    config: {
        model: 'app.model.Quiz',
        storeId: 'quizList',
        data: [{
            id: 1,
            log: '',
            title: '上网行为调查',
            integral: 20,
            winning: 2,
            count: 100,
            qCount: 20,
            log: 'lib/img/weather/n20.gif'
        },
        {
            id: 2,
            log: '',
            title: '个人消费调查',
            integral: 10,
            winning: 1,
            count: 100,
            qCount: 20,
            post: 'xx公司',
            log: 'lib/img/weather/n20.gif'
        },
        {
            id: 3,
            log: '',
            title: 'xxx公司产品调查',
            integral: 10,
            winning: 1,
            qCount: 20,
            log: 'lib/img/weather/n20.gif'
        },{
            id: 4,
            log: '',
            title: '个人信息调查',
            integral: 50,
            winning: 5,
            qCount: 20,
            log: 'lib/img/weather/n20.gif'
        }]
    }
});