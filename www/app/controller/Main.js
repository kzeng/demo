﻿/*
*视图切换控制
*/
Ext.define('app.controller.Main', {
    extend: 'Ext.app.Controller',
    config: {
        models: ['Config'],
        views: ['FirstStart', 'Main'],
        refs: {
            main: 'main',
            guide: 'guide',
            firstStart: 'firstStart',
            redirectBtn: 'button[action=redirect]',
            otherBtn: 'button[action=other]'
        },
        before: {
        //            showMian: 'ckFirst'
    },
    control: {
        firstStart: {
            showMain: 'onGuide'
        },
        guide: {
            showMain: 'onGuide'
        },
        //跳转按钮
        redirectBtn: {
            tap: function (t, value) {
                this.redirectTo('redirec/' + t['redirect']);
            }
        },
        //其他按钮
        otherBtn: {
            tap: function () {
                util.showMessage('我是一个toast提示- -', true);
            }
        }
    },
    //路由，由redirectTo方法触发
    routes: {
        'main': 'showMian',
        'redirec/:view': 'redirec',
        'firstStart': 'showFirstStart'
    }
},
launch: function () {
    //检测是否第一次启动程序
    Ext.ModelMgr.getModel('app.model.Config').load(1, {
        scope: this,
        success: function (config) {
            this.redirectTo('main');
        },
        failure: function (error) {
            this.redirectTo('firstStart');
        }
    });
},
//显示欢迎页面
showFirstStart: function () {
    console.log('首次启动,进入欢迎页面');
    util.ePush('firstStart');
    //存储配置信息
    var config = Ext.create('app.model.Config', {
        id: 1
    });
    config.save();
},
//显示首页
showMian: function () {
    console.log('进入首页');
    util.ePush('main');
},
//跳转到首页路由
onGuide: function () {
    this.redirectTo('main');
},
//显示视图xtype:这里是指alternateClassName
redirec: function (xtype) {
    var main = this.getMain();
    if (main) {
        main.push(xtype);
    }
}
});