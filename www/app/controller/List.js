﻿/*
*列表控制
*/
Ext.define('app.controller.List', {
    extend: 'Ext.app.Controller',
    config: {
        models: ['Blog', 'Quiz'],
        stores: ['BlogList', 'QuizList'],
        views: ['list.Home'],
        refs: {
            listHome: 'listHome'
        },
        control: {
            listHome: {
                itemtap: function (list, index, target, record, e) {
                    this.redirectTo('redirec/' + record.get('redirect'));
                    util.storeLoad(record.get('store'));
                }
            }
        }
    }
});