﻿/*
*面板控制
*/
Ext.define('app.controller.Panel', {
    extend: 'Ext.app.Controller',
    config: {
        views: ['panel.List'],
        refs: {
            panelList: 'panelList'
        },
        control: {
            panelList: {
                itemtap: function (list, index, target, record, e) {
                    this.redirectTo('redirec/' + record.get('redirect'));
                }
            }
        }
    }
});