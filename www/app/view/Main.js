﻿/*
*主视图,负责视图切换
*/
Ext.define('app.view.Main', {
    alternateClassName: 'main',
    extend: 'ux.CardPanel',
    requires: ['app.view.Home', 'app.view.MyBar'],
    xtype: 'main',
    config: {
        cls: 'cardPanel',
        backBtn: {
            iconCls: 'arrow_left',
            ui: '',
            cls:'back'
        },
        //items中的项只能配置一个
        items:[{xtype:'home'}] 
    }
});
