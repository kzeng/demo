Ext.define('app.view.About', {
    alternateClassName: 'about',
    extend: 'Ext.Container',
    xtype: 'about',
    config: {
        title: '关于',
        cls: 'info',
        otherMenu: 'myBar',
        backHide:true,
        html: '这是一个开源示例,是我对sencha touch的深层应用.'
    }
});