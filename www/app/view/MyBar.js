﻿Ext.define('app.view.MyBar', {
    alternateClassName: 'myBar',
    extend: 'ux.TabBar',
    xtype: 'myBar',
    requires: ['app.view.About'],
    config: {
        items: [
        {
            xtype: 'button',
            text: '首页',
            //只有第一个设置的属性有效
            selected: true,
            action: 'redirect',
            redirect: 'home'
        },
        {
            xtype: 'button',
            text: '关于',
            action: 'redirect',
            redirect: 'about'
        },
        {
            xtype: 'button',
            text: '其他',
            //没有选中效果
            noSelect:true,
            action: 'other'
        }]
    }
});