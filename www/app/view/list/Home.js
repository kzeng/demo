Ext.define('app.view.list.Home', {
    alternateClassName: 'listHome',
    extend: 'Ext.List',
    xtype: 'listHome',
    requires: ['app.view.list.Xml','app.view.list.Tpl'],
    config: {
        title: '列表',
        scrollable: {
            disabled: true
        },
        itemTpl: '{title}',
        data: [{
            title: 'Xml取值',
            redirect: 'listXml',
            store: 'blogList'
        }, {
            title: '列表模版',
            redirect: 'listTpl',
            store: 'quizList'
        }]
    }
});