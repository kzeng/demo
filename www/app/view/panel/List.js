Ext.define('app.view.panel.List', {
    alternateClassName: 'panelList',
    extend: 'Ext.List',
    xtype: 'panelList',
    requires: ['app.view.panel.Info', 'app.view.panel.CInfo', 'app.view.panel.Href'],
    config: {
        title: '面板列表',
        itemTpl: '{title}',
        scrollable: {
            disabled: true
        },
        data: [{
            title: '下拉刷新',
            redirect: 'panelInfo'
        }, {
            title: 'Container模版',
            redirect: 'panelCInfo'
        }, {
            title: '内容包含超链接',
            redirect: 'panelHref'
        }]
    }
});