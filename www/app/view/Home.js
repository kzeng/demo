﻿Ext.define('app.view.Home', {
    extend: 'Ext.Container',
    xtype: 'home',
    config: {
        title: '首页',
        //额外的按钮组，只能是按钮组。不能添加其他属性
        navigationItem: [{
            xtype: 'container',
            cls: 'weather',
            align: 'right',
            itemId: 'weather',
            tpl: '<div class="dc"><span class="pIco" style="background-image:url(\'lib/img/weather/{img1}\')"></span>{temp1}</div><div class="tc">{weather}</div>',
            listeners: {
                initialize: function (t) {
                    if (config.weatherinfo) {
                        t.setData(config.weatherinfo);
                    } else {
                        Ext.Ajax.request({
                            url: config.weather,
                            hidMessage: true,
                            success: function (response, request) {
                                config.weatherinfo = Ext.decode(response.responseText).weatherinfo;
                                if (t) {
                                    t.setData(config.weatherinfo);
                                }
                            }
                        });
                    }
                }
            }
        }],
        otherMenu: 'myBar',
        isPopAll: true,
        cls: 'home',
        layout: 'vbox',
        defaults: {
            layout: 'hbox',
            defaults: {
                flex: 1
            }
        },
        items: [{
            items: [{
                xtype: 'button',
                text: '九宫格',
                iconAlign: 'top',
                iconCls: 'squared orangeYellow',
                action: 'redirect',
                redirect: 'layoutSquared'
            },
            {
                xtype: 'button',
                text: '面板',
                iconAlign: 'top',
                iconCls: 'organize orange',
                action: 'redirect',
                redirect: 'panelList'
            },
            {
                xtype: 'button',
                text: '列表',
                iconAlign: 'top',
                iconCls: 'list roseRed',
                action: 'redirect',
                redirect: 'listHome'
            }, {
                xtype: 'button',
                iconAlign: 'top',
                iconCls: 'refresh lightBlue',
                text: '按钮4'
            }]
        },
        {
            items: [
            {
                xtype: 'button',
                iconAlign: 'top',
                iconCls: 'search green',
                text: '按钮5'
            },
            {
                xtype: 'button',
                iconAlign: 'top',
                iconCls: 'settings blue',
                text: '按钮6'
            }, {
                xtype: 'button',
                iconAlign: 'top',
                iconCls: 'star yellow',
                text: '按钮7'
            },
            {
                xtype: 'button',
                iconAlign: 'top',
                iconCls: 'trash paleYellow',
                text: '按钮8'
            }]
        },
        {
            width: '25%',
            items: [
            {
                xtype: 'button',
                iconAlign: 'top',
                iconCls: 'maps smBlue',
                text: '按钮9'
            }]
        }]
    }
});