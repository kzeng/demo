﻿/*
*模仿且改进NavigationView
*返回时自动销毁视图，节约内存
*/
Ext.define('ux.CardPanel', {
    extend: 'Ext.Container',
    xtype: 'cardPanel',
    requires: ['Ext.TitleBar'],
    config: {
        //布局，type不可更改
        layout: {
            type: 'card',
            animation: {
                duration: 300,
                easing: 'ease-out',
                type: 'slide',
                direction: 'left'
            }
        },
        //默认导航条
        navigationBar: {
            docked: 'top'
        },
        //顶部控件组，在子项中配置
        navigationItem: null,
        //顶部控件组cls，在子项中配置
        navigationCls: null,
        //返回按钮
        backBtn: {
            align: 'left',
            ui: 'back'
        },
        //返回按钮显示状态，在子项中配置
        backHide: false,
        //其他导航栏、可选格式otherMenu:'xtype'，在子项中配置
        otherMenu: null,
        //移除第一项和当前项之外的所有项，在子项中配置isPopAll：true
        isPopAll: null
    },
    //初始化配置
    initConfig: function () {
        //console.log('初始化配置');
        //历史记录
        this.viewStack = [];
        //视图加载状态
        this.viewStatus = {};
        this.callParent(arguments);
    },
    //初始化
    initialize: function () {
        var layout = this.getLayout();
        if (layout && !layout.isCard) {
            Ext.Logger.error('CardPanel中layout的布局只能是card布局');
        }
    },
    //更新标题
    setTitle: function (title) {
        this.getNavigationBar().setTitle(title);
    },
    //更新返回按钮显示状态
    updateBackHide: function (newItem, oldItem) {
        var backBtn = this.getBackBtn();
        if (newItem) {
            backBtn.hide();
        } else {
            backBtn.show();
        }
    },
    //更新其他导航栏
    updateOtherMenu: function (newItem, oldItem) {
        if (oldItem) {
            oldItem = this.down(oldItem);
            this.remove(oldItem);
        }
        if (newItem) {
            newItem = Ext.create(newItem);
            this.add(newItem);
        }
    },
    //创建导航栏控件组
    applyNavigationItem: function (newItems) {
        if (!newItems) return false;
        var me = this,
        navItems = [],
        i,
        ln;
        newItems = Ext.Array.from(newItems);
        for (i = 0, ln = newItems.length; i < ln; i++) {
            navItems.push(me.factoryItem(newItems[i]));
        }
        return navItems;
    },
    //更新导航栏控件组
    updateNavigationItem: function (newItem, oldItem) {
        var navigationBar = this.getNavigationBar();
        if (oldItem) {
            var i, ln;
            for (i = 0, ln = oldItem.length; i < ln; i++) {
                navigationBar.remove(oldItem[i]);
            }
        }
        if (newItem) {
            navigationBar.add(newItem);
        }
    },
    //创建顶部导航栏
    applyNavigationBar: function (config) {
        return Ext.factory(config, Ext.TitleBar, this.getNavigationBar());
    },
    //更新顶部导航栏
    updateNavigationBar: function (newItem, oldItem) {
        if (oldItem) {
            this.remove(oldItem);
        }
        if (newItem) {
            this.add(newItem);
        }
    },
    //更新临时cls
    updateNavigationCls: function (newItem, oldItem) {
        if (oldItem) {
            this.removeCls(oldItem);
        }
        if (newItem) {
            this.addCls(newItem);
        }
    },
    //创建返回按钮
    applyBackBtn: function (config) {
        return Ext.factory(config, Ext.Button, this.getBackBtn());
    },
    //更新返回按钮
    updateBackBtn: function (newItem, oldItem) {
        if (oldItem) {
            this.getNavigationBar().remove(oldItem);
        }
        if (newItem) {
            this.getNavigationBar().add(newItem);
            newItem.on({
                scope: this,
                tap: this.onBackButtonTap
            });
        }
    },
    //更新移除
    updateIsPopAll: function (newItem, oldItem) {
        var animation = this.getLayout().getAnimation();
        //添加监听
        if (oldItem) {
            //移除动画结束监听
            animation.un({
                scope: this,
                animationend: 'onAnimationend'
            });
        }
        if (newItem) {
            this.viewStack = [this.viewStack[0]];
            //添加动画结束监听
            animation.on({
                scope: this,
                animationend: 'onAnimationend'
            });
        }
    },
    /**
    * 移除历史记录
    * @private
    */
    viewStackPop: function (count) {
        for (var i = 0; i < count; i++) {
            this.viewStack.pop();
        }
    },
    //添加视图
    //注意xtype是指alternateClassName
    push: function (xtype, params) {
        var me = this,
        view = me.getActiveItem(),
        oldXtype = view && (view.config.xtype || view.getItemId());
        me.tempConfig = params;
        /*过滤已经添加的视图*/
        if (!me.viewStatus[xtype]) {
            view = Ext.create(xtype, { itemId: xtype });
            me.add(view);
        } else if (oldXtype != xtype) {
            me.viewStack.push(xtype);
            me.setActiveItem(xtype);
        } else if (params) {
            this.onActivate(view);
        }
    },
    //当动画效果结束时,子项配置了isPopAll:true属性将会激活
    onAnimationend: function () {
        //        console.log('animationend');
        this.popAll();
    },
    //项被激活
    onActivate: function (view) {
        if (this.tempConfig) {
            view.setConfig(this.tempConfig);
            delete this.tempConfig;
        }
        var config = view.config;
        //更新需要移除的项
        this.setIsPopAll(config.isPopAll || false);
        //        console.log('activate', config.xtype || view.getItemId());
        //更新后退按钮
        //        console.log('setBackHide', this.viewStack);
        this.setBackHide(config.backHide || this.viewStack.length == 1);
        //更新导航栏控件组
        this.setNavigationItem(config.navigationItem || false);
        //更新导航栏cls
        this.setNavigationCls(config.navigationCls || false);
        //更新标题
        this.setTitle(config.title || '');
        //更新其他导航栏
        this.setOtherMenu(config.otherMenu || false);
    },
    //项被销毁
    onDestroy: function (view) {
        //        console.log('onDestroy', view.config.xtype || view.getItemId());
        this.viewStatus[view.config.xtype || view.getItemId()] = false;
    },
    /**
    * 不填写参数时，移除当前项，返回到上一项
    * 如果参数是数字，则从最后一项开始移除指定数目的项
    * 如果参数是string,则移除指定类型的项
    * 如果参数是项，则移除传入的项
    * 不论参数如何，都会保留一个活动项
    * @return {Ext.Component} 当前活动项
    */
    pop: function (count) {
        if (this.beforePop(count)) {
            return this.doPop();
        }
    },
    /**
    * @private
    *删除指定项
    */
    beforePop: function (count) {
        var me = this,
        innerItems = me.getInnerItems();
        if (Ext.isString(count) || Ext.isObject(count)) {
            var last = innerItems.length - 1,
            i;
            for (i = last; i >= 0; i--) {
                if ((Ext.isString(count) && Ext.ComponentQuery.is(innerItems[i], count)) || (Ext.isObject(count) && count == innerItems[i])) {
                    //获得移除项序号
                    count = last - i;
                    break;
                }
            }
            if (!Ext.isNumber(count)) {
                return false;
            }
        }
        var ln = innerItems.length,
        toRemove;
        //默认移除一项
        if (!Ext.isNumber(count) || count < 1) {
            count = 1;
        }
        //当我们试图移除更多视图时
        count = Math.min(count, ln - 1);
        if (count) {
            this.viewStackPop(count);
            //开始移除视图
            toRemove = innerItems.splice(-count, count - 1);
            for (i = 0; i < toRemove.length; i++) {
                this.remove(toRemove[i]);
            }
            return true;
        }
        return false;
    },
    /**
    * @private
    *移除最后一项
    */
    doPop: function () {
        var me = this,
        innerItems = this.getInnerItems(),
        ord = innerItems[innerItems.length - 1];
        me.remove(ord);
        //触发被移除项的事件
        return this.getActiveItem();
    },
    doResetActiveItem: function (innerIndex) {
        var me = this,
        innerItems = me.getInnerItems(),
        animation = me.getLayout().getAnimation();
        if (innerIndex > 0) {
            if (animation && animation.isAnimation) {
                animation.setReverse(true);
            }
            me.setActiveItem(innerIndex - 1);
        }
    },
    /**
    * @private 
    *执行移除项，调用remove方法后自动执行
    */
    doRemove: function () {
        var animation = this.getLayout().getAnimation();
        if (animation && animation.isAnimation) {
            animation.setReverse(false);
        }
        this.callParent(arguments);
    },
    /**
    * @private 
    *执行添加项，调用add方法后自动执行
    */
    onItemAdd: function (item, index) {
        if (item.isInnerItem()) {
            var xtype = item.config.xtype || item.getItemId();
            this.viewStatus[xtype] = true;
            this.viewStack.push(xtype);
            //添加监听
            item.on({
                scope: this,
                activate: 'onActivate',
                destroy: 'onDestroy'
            });
        }
        this.doItemLayoutAdd(item, index);
        if (!this.isItemsInitializing && item.isInnerItem()) {
            this.setActiveItem(item);
        }
        if (this.initialized) {
            this.fireEvent('add', this, item, index);
        }
    },
    /**
    * 移除第一项和最后项之间的所有项（包括最后项）
    * @return {Ext.Component} 当前活动视图
    */
    reset: function () {
        return this.pop(this.getInnerItems().length);
    },
    //移除第一项和当前项之外的所有项
    popAll: function () {
        var me = this,
        innerItems = this.getInnerItems(),
        length = innerItems.length-1,
        ordItem,
        i;
        for (i = length; i > 0; i--) {
            ordItem = innerItems[i];
            if (ordItem.isHidden()) {
                me.remove(ordItem, true);
            } else {
                me.viewStack.push(ordItem.config.xtype || ordItem.getItemId());
            }
        }
    },
    //返回上一个历史记录
    onBackButtonTap: function () {
        this.pop();
        this.fireEvent('back', this);
    }
});